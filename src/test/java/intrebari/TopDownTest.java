package intrebari;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TopDownTest {

    IntrebariRepository intrebariRepository = new IntrebariRepository();
    AppController appController = new AppController(intrebariRepository);
    private String file = "./src/testfile.txt";

    @Test
    public void test1() throws InputValidationFailedException, DuplicateIntrebareException {
        //P->A
        String enunt = "Estee?";
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        String domeniu = "Informatica";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu);
        intrebariRepository.addIntrebare(intrebare, file);
        assertEquals(1, intrebariRepository.getIntrebari().size());
    }

    @org.junit.Test
    public void test2() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateTestException {
        //P->A->B
        System.out.println(this.getClass());
        addManyQuestions(100);
        evaluator.model.Test test = appController.createNewTest();
        assertEquals(5, test.getIntrebari().size());
    }

    @org.junit.Test
    public void test3() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {
        //P->A->C->B
        addManyQuestions(100);
        addManyQuestionsWithDomain(10, "Informatica");
        assertEquals(110, intrebariRepository.getIntrebari().size());
        Statistica statistica = appController.getStatisticsByDomeniu("Informatica");
        assertEquals(Integer.valueOf(10), statistica.getIntrebariDomenii().get("Informatica"));
        evaluator.model.Test test = appController.createNewTest();
        assertEquals(5, test.getIntrebari().size());
    }

    private void addManyQuestionsWithDomain(int max, String domain) throws InputValidationFailedException, DuplicateIntrebareException {
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Maybe.";
        String varCorecta = "3";
        for (int i = 0; i < max; i++) {
            String enunt = "Este " + i + "?";
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domain), file);
        }

    }

    private void addManyQuestions(int max) throws InputValidationFailedException, DuplicateIntrebareException {
        String varianta1 = "1)Dap.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        for (int i = 0; i < max; i++) {
            String enunt = "Este " + i + "?";
            String domeniu = "Informatica" + i;
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
        }
    }
}
