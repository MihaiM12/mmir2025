package intrebari;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class GetStatisticaTest {

    private IntrebariRepository intrebariRepository = new IntrebariRepository();
    private AppController appController = new AppController(intrebariRepository);
    private String file = "./src/testfile.txt";
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void getStatisticaByDomeniu() {
        intrebariRepository.loadIntrebariFromFile(file);
        String domeniu = "Psychology";
        try {
            Statistica statistica = appController.getStatisticsByDomeniu(domeniu);
        } catch (NotAbleToCreateStatisticsException e) {
            System.out.println("Bad Domain!");
        }
    }

    @Test
    public void getStatisticaByDomeniuBad() {
        intrebariRepository.loadIntrebariFromFile(file);
        String domeniu = "Wont exist";
        try {
            Statistica statistica = appController.getStatisticsByDomeniu(domeniu);
            System.out.println(!(statistica.getIntrebariDomenii().get(domeniu) == 0) ? statistica : "Bad Domain");
        } catch (NotAbleToCreateStatisticsException e) {
            System.out.println("Something went wrong");
        }
    }
}
