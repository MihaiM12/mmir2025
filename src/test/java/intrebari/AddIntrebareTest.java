package intrebari;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class AddIntrebareTest {

    private IntrebariRepository intrebariRepository = new IntrebariRepository();
    private AppController appController = new AppController(intrebariRepository);
    private String file = "./src/testfile.txt";
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void addIntrebare() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "Este?";
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        String domeniu = "Informatica";
        intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
    }

    @Test
    public void addIntrebareLimitDomain() throws InputValidationFailedException, DuplicateIntrebareException {
        String enunt = "Este?";
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        String domeniu = "InformaticaaaInformaticaaaaaaa";
        intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
    }

    @Test
    public void addIntrebareDuplicate() throws InputValidationFailedException {
        System.out.println(this.getClass());
        String enunt = "Este?";
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        String domeniu = "Informatica";
//        exception.expect(DuplicateIntrebareException.class);
        try {
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
        } catch (DuplicateIntrebareException e) {
            assert (e.getMessage().equals("Intrebarea deja exista!"));
        }

    }

    @Test
    public void badOption() throws DuplicateIntrebareException {
        String enunt = "Este?";
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "";
        String varCorecta = "3";
        String domeniu = "Informatica";
        try {
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
        } catch (InputValidationFailedException e) {
            assert (e.getMessage().equals("Varianta3 este vida!"));
        }
    }

    @Test
    public void badEnunt() throws DuplicateIntrebareException {
        String enunt = "Este";
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate";
        String varCorecta = "3";
        String domeniu = "Informatica";
        try {
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
        } catch (InputValidationFailedException e) {
            assert (e.getMessage().equals("Ultimul caracter din enunt nu e '?'!"));
        }
    }

    @Test
    public void badDomeniu() throws DuplicateIntrebareException {
        String enunt = "Este?";
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate";
        String varCorecta = "3";
        String domeniu = "informatica";
        try {
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
        } catch (InputValidationFailedException e) {
            assert (e.getMessage().equals("Prima litera din domeniu nu e majuscula!"));
        }

    }

    @Test
    public void questionTooLong() throws DuplicateIntrebareException {
        String enunt = "Esssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssste?";
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        String domeniu = "Informatica";
        try {
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
        } catch (InputValidationFailedException e) {
            assert (e.getMessage().equals("Lungimea enuntului depaseste 100 de caractere!"));
        }
    }

    @Test
    public void optionTooLong() throws DuplicateIntrebareException {
        String enunt = "Este??";
        String varianta1 = "1)Daaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        String domeniu = "Informatica";
        try {
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
        } catch (InputValidationFailedException e) {
            assert (e.getMessage().equals("Lungimea variantei1 depaseste 50 de caractere!"));
        }
    }

    @Test
    public void domainTooLong() throws DuplicateIntrebareException {
        String enunt = "Este??";
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        String domeniu = "InformaticaInformaticaInformatica";
        try {
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
        } catch (InputValidationFailedException ex) {
            assert (ex.getMessage().equals("Lungimea domeniului depaseste 30 de caractere!"));
        }
    }
}
