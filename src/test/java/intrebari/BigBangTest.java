package intrebari;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BigBangTest {
    private IntrebariRepository intrebariRepository = new IntrebariRepository();
    private AppController appController = new AppController(intrebariRepository);
    private String file = "./src/testfile.txt";

    @Test
    public void test1() throws InputValidationFailedException, DuplicateIntrebareException {
        //P->A
        String enunt = "Este?";
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        String domeniu = "Informatica";
        Intrebare intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu);
        intrebariRepository.addIntrebare(intrebare, file);
        assertEquals(1, intrebariRepository.getIntrebari().size());
    }

    @Test
    public void test2() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateTestException {
        //P->B
        addManyQuestions(4);
        evaluator.model.Test test = appController.createNewTest();
        assertEquals(test.getIntrebari().size(), 5);
    }

    @Test
    public void test3() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateStatisticsException {
        //P->C
        addManyQuestionsWithDomain(10, "Informatica");
        addManyQuestionsWithDomain(43, "Matematica");
        addManyQuestionsWithDomain(17, "Engleza");
        Statistica statistica = appController.getStatisticsByDomeniu("Informatica");
        assertEquals(Integer.valueOf(10), statistica.getIntrebariDomenii().get("Informatica"));
        statistica = appController.getStatisticsByDomeniu("Matematica");
        assertEquals(Integer.valueOf(43), statistica.getIntrebariDomenii().get("Matematica"));
        statistica = appController.getStatisticsByDomeniu("Engleza");
        assertEquals(Integer.valueOf(17), statistica.getIntrebariDomenii().get("Engleza"));
    }

    @Test
    public void test4() throws DuplicateIntrebareException, InputValidationFailedException, NotAbleToCreateTestException, NotAbleToCreateStatisticsException {
        //P->A->B->C
        addManyQuestionsWithDomain(10, "Informatica");
        addManyQuestions(4);
        evaluator.model.Test test = appController.createNewTest();
        assertEquals(test.getIntrebari().size(), 5);
        Statistica statistica = appController.getStatisticsByDomeniu("Informatica");
        assertEquals(Integer.valueOf(10), statistica.getIntrebariDomenii().get("Informatica"));
    }

    private void addManyQuestionsWithDomain(int max, String domain) throws InputValidationFailedException, DuplicateIntrebareException {
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        for(int i = 0; i < max; i++){
            String enunt = "Este "+i+ "?";
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domain), file);
        }

    }

    private void addManyQuestions(int max) throws InputValidationFailedException, DuplicateIntrebareException {
        String varianta1 = "1)Daa.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        for(int i = 0; i <= max; i++){
            String enunt = "Este "+i+ "?";
            String domeniu = "Informatica"+i;
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
        }
    }
}
