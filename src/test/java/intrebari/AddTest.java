package intrebari;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.repository.IntrebariRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class AddTest {

    private IntrebariRepository intrebariRepository = new IntrebariRepository();
    private AppController appController = new AppController(intrebariRepository);
    private String file = "./src/testfile.txt";
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void addIntrebareToTest() throws InputValidationFailedException, NotAbleToCreateTestException, DuplicateIntrebareException {
        addManyQuestions(7);
        evaluator.model.Test test = appController.createNewTest();
        appController.addIntrebareToTest(test);
        assertEquals(5, test.getIntrebari().size());
    }

    @Test
    public void addIntrebareToTestAgain() throws InputValidationFailedException, NotAbleToCreateTestException, DuplicateIntrebareException {
        addManyQuestions(4);
        evaluator.model.Test test = appController.createNewTest();
        appController.addIntrebareToTest(test);
    }

    @Test
    public void addIntrebateToTestBadTestAgain() throws DuplicateIntrebareException, InputValidationFailedException {
        addManyQuestions(0);
        try {
            evaluator.model.Test test = appController.createNewTest();
            if (appController.addIntrebareToTest(test) == null)
                System.out.println("Bad test input");
        } catch (NotAbleToCreateTestException ex) {
            System.out.println("Too few questions to create a test");
        }
    }

    @Test
    public void addIntrebateToTestBadTest() throws DuplicateIntrebareException, InputValidationFailedException {
        addManyQuestions(3);
        try {
            evaluator.model.Test test = appController.createNewTest();
            appController.addIntrebareToTest(test);
        } catch (NotAbleToCreateTestException ex) {
            System.out.println("The test is not valid, not enough questions");
        }
    }

    private void addManyQuestions(int max) throws InputValidationFailedException, DuplicateIntrebareException {
        String varianta1 = "1)Da.";
        String varianta2 = "2)Nu.";
        String varianta3 = "3)Poate.";
        String varCorecta = "3";
        for (int i = 0; i <= max; i++) {
            String enunt = "Este " + i + "?";
            String domeniu = "Informatica" + i;
            intrebariRepository.addIntrebare(new Intrebare(enunt, varianta1, varianta2, varianta3, varCorecta, domeniu), file);
        }
    }
}
