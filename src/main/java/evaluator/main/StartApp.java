package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.repository.IntrebariRepository;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "./src/intrebari.txt";
	
	public static void main(String[] args) throws IOException, DuplicateIntrebareException, NotAbleToCreateTestException, InputValidationFailedException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

		IntrebariRepository intrebariRepository = new IntrebariRepository();
		AppController appController = new AppController(intrebariRepository);
		
		boolean activ = true;
		String optiune = null;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = console.readLine();
			
			switch(optiune){
			case "1" :
				//break;
				appController.addNewIntrebare(intrebareOptions(), file);
				break;
			case "2" :
				//break;
				appController.loadIntrebariFromFile(file);
				appController.createNewTest();
				break;
			case "3" :
				appController.loadIntrebariFromFile(file);
				Statistica statistica;
				try {
					statistica = appController.getStatistica();
					System.out.println(statistica);
				} catch (NotAbleToCreateStatisticsException e) {
					// TODO 
				}
				
				break;
			case "4" :
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

	public static Intrebare intrebareOptions() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		Intrebare intrebare = new Intrebare();
		System.out.println("Intrebare");
		try {
			//intrebare.setEnunt(br.readLine());
			String enunt = br.readLine();
			System.out.println("Optiune 1");
			//intrebare.setVarianta1(br.readLine());
			String var1 = br.readLine();
			System.out.println("Optiune 2");
			//intrebare.setVarianta2(br.readLine());
			String var2 = br.readLine();
			System.out.println("Optiune 3");
			//intrebare.setVarianta3(br.readLine());
			String var3 = br.readLine();
			System.out.println("Optiune corecta");
			//intrebare.setVariantaCorecta(br.readLine());
			String varC = br.readLine();
			System.out.println("Domeniu");
			//intrebare.setDomeniu(br.readLine());
			String domeniu = br.readLine();
			try {
				intrebare = new Intrebare(enunt, var1, var2, var3, varC, domeniu);
			}catch(Exception ex){
				System.out.println(ex.getMessage());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return intrebare;

	}

}
